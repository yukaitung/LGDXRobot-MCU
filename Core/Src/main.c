/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "pca9685.h"
#include "VL53L0X.h"
#include "I2CDev.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
// Math
#define PI 3.14159265359
	
// CHASSIS CONFIGURE 
// SI Unit
#define LX 0.1
#define LY 0.105
#define WHEEL_RADIUS 0.04
#define GEAR_RATIO 30
#define WHEEL_COUNT 4

// Encoder
/*
	The encoder is 11 PPR
	PPR for wheel is 330 (PPR * gear ratio)
	Each revolution of wheel generates 1320 pulse
	And each pulse = 2pi/1320 = 0.00475998886 radian/pulse
*/
#define RADIAN_PER_PULSE 0.00475998886

// UART
#define BUFFER_SIZE 110
#define UART_DMA_BUFFER_SIZE 2048
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;
I2C_HandleTypeDef hi2c2;

TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim4;
TIM_HandleTypeDef htim5;

UART_HandleTypeDef huart2;
DMA_HandleTypeDef hdma_usart2_rx;

/* Definitions for UARTParserTask */
osThreadId_t UARTParserTaskHandle;
const osThreadAttr_t UARTParserTask_attributes = {
  .name = "UARTParserTask",
  .priority = (osPriority_t) osPriorityRealtime,
  .stack_size = 128 * 4
};
/* Definitions for PrintStatusTask */
osThreadId_t PrintStatusTaskHandle;
const osThreadAttr_t PrintStatusTask_attributes = {
  .name = "PrintStatusTask",
  .priority = (osPriority_t) osPriorityLow,
  .stack_size = 128 * 4
};
/* Definitions for ReadLaserTask */
osThreadId_t ReadLaserTaskHandle;
const osThreadAttr_t ReadLaserTask_attributes = {
  .name = "ReadLaserTask",
  .priority = (osPriority_t) osPriorityLow,
  .stack_size = 128 * 4
};
/* Definitions for UpdateMotorTask */
osThreadId_t UpdateMotorTaskHandle;
const osThreadAttr_t UpdateMotorTask_attributes = {
  .name = "UpdateMotorTask",
  .priority = (osPriority_t) osPriorityRealtime1,
  .stack_size = 128 * 4
};
/* USER CODE BEGIN PV */
// Inverse kinematics
static float motor_target_wVelocity[WHEEL_COUNT] = {0};
static int motor_duty_cycle[WHEEL_COUNT] = {0};
static int motor_last_duty_cycle[WHEEL_COUNT] = {0};
static int motor_forward[WHEEL_COUNT] = {0};

// Encoder
static int encoder_current[WHEEL_COUNT] = {0};
static int encoder_last[WHEEL_COUNT] = {0};
static float encoder_wVelocity[WHEEL_COUNT] = {0};
static float encoder_last_wVelocity[WHEEL_COUNT] = {0};
const float ENCODER_MAX_VELOCITY = 100;

// PID
const int kMaxError = 500;
const float kWheelToPCA = 105.263157895; // 4000 pulse / 38 (highest speed)
static float motor_last_error[WHEEL_COUNT] = {0};
static float motor_sum_error[WHEEL_COUNT] = {0};
static float motor_kp[WHEEL_COUNT] = {5, 5, 8, 6};
static float motor_ki[WHEEL_COUNT] = {0, 0, 0, 0};
static float motor_kd[WHEEL_COUNT] = {1, 1, 1, 1};
const float ERROR_TOLERANT = RADIAN_PER_PULSE; // RAD

// UART
static uint8_t uart_rx_buffer[UART_DMA_BUFFER_SIZE];

// Laser
//uint8_t i,scaner;
VL53L0X sensor1;
VL53L0X sensor2;
VL53L0X sensor3;
uint16_t laser_value[3] = {0};
#define ENABLE_LASER1
#define ENABLE_LASER2
#define ENABLE_LASER3

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_I2C1_Init(void);
static void MX_I2C2_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM3_Init(void);
static void MX_TIM4_Init(void);
static void MX_TIM5_Init(void);
static void MX_USART2_UART_Init(void);
void UARTParser(void *argument);
void PrintStatus(void *argument);
void readLaser(void *argument);
void updateMotor(void *argument);

/* USER CODE BEGIN PFP */
void inverse_kinematics(float vx, float vy, float w);
void update_motor_pid(int motor, float kp, float ki, float kd);
void update_motor_direction();
void update_servo_angle(int servo, float angle);
void changeAddresses(void); //To turn on connected VL53L0X sensors one at a time, change their address and initialise them.
int safe_unsigned_subtract(int a, int b, int limit);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void I2C_Read_Byte(uint8_t addr, uint8_t * data) {
  while (HAL_I2C_GetState( & hi2c2) != HAL_I2C_STATE_READY);
  if (HAL_I2C_Master_Transmit( & hi2c2, SLAVE_ADDRESS << 1, & addr, 1, 100) != HAL_OK) {
    Error_Handler();
  }
  while (HAL_I2C_GetState( & hi2c2) != HAL_I2C_STATE_READY);
  if (HAL_I2C_Master_Receive( & hi2c2, ((SLAVE_ADDRESS << 1) | 0x01), data, 1, 100) != HAL_OK) {
    Error_Handler();
  }
}

void I2C_Write_Byte(uint8_t addr, uint8_t data) {
  uint8_t buf[] = {
    addr,
    data
  };
  while (HAL_I2C_GetState( & hi2c2) != HAL_I2C_STATE_READY);
  if (HAL_I2C_Master_Transmit( & hi2c2, SLAVE_ADDRESS << 1, buf, 2, 100) != HAL_OK) {
    Error_Handler();
  }
}

void readMulti(VL53L0X * lidar, uint8_t reg, uint8_t * dst, uint8_t count) {
  /* Send register address */
  if (HAL_I2C_Master_Transmit( & hi2c2, SLAVE_ADDRESS << 1, & reg, 1, 1000) != HAL_OK) {
    {
      Error_Handler();
    }
  }

  /* Receive multiple byte */
  if (HAL_I2C_Master_Receive( & hi2c2, ((SLAVE_ADDRESS << 1) | 0x01), dst, count, 1000) != HAL_OK) {
    Error_Handler();
  }
}

void writeMulti(VL53L0X * lidar, uint8_t reg, uint8_t * src, uint8_t count) {
  /* Try to transmit via I2C */
  if (HAL_I2C_Mem_Write( & hi2c2, SLAVE_ADDRESS << 1, reg, reg > 0xFF ? I2C_MEMADD_SIZE_16BIT : I2C_MEMADD_SIZE_8BIT, src, count, 1000) != HAL_OK) {
    Error_Handler();
  }
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_I2C1_Init();
  MX_I2C2_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_TIM4_Init();
  MX_TIM5_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */
	sprintf(msg,"Start System\r\n");
  HAL_UART_Transmit(&huart2, (uint8_t*)msg, strlen(msg), 0xFFFF);
	
	// Enable DMA
	HAL_UART_Receive_DMA (&huart2, uart_rx_buffer, UART_DMA_BUFFER_SIZE);
		
	// Initialize encoder
	HAL_TIM_Encoder_Start(&htim2, TIM_CHANNEL_ALL);
	HAL_TIM_Encoder_Start(&htim3, TIM_CHANNEL_ALL);       
	HAL_TIM_Encoder_Start(&htim4, TIM_CHANNEL_ALL);       
	HAL_TIM_Encoder_Start(&htim5, TIM_CHANNEL_ALL); 
	
	// Initialize PCA9685
	PCA9685_Init(&hi2c1);
	HAL_Delay(200);
	// Motor
	PCA9685_SetPin(0, 0, 0);
	PCA9685_SetPin(1, 0, 0);
	PCA9685_SetPin(2, 0, 0);
	PCA9685_SetPin(3, 0, 0);
	// Servo
	PCA9685_SetServoAngle(4, 0);
	PCA9685_SetServoAngle(5, 0);
	PCA9685_SetServoAngle(6, 0);
	PCA9685_SetServoAngle(7, 0);
	PCA9685_SetServoAngle(15, 0);
	HAL_Delay(200);
	
	// Stop Chassis
	// D1
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_RESET); 
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, GPIO_PIN_RESET); 
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, GPIO_PIN_SET); 
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_RESET); 
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, GPIO_PIN_RESET); 
	// D2
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_2, GPIO_PIN_RESET); 
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_3, GPIO_PIN_RESET); 
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_15, GPIO_PIN_SET); 
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_14, GPIO_PIN_RESET); 
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_13, GPIO_PIN_RESET); 
	
	// Laser
	setup_VL53L0X(&sensor1);
	setup_VL53L0X(&sensor2);
	setup_VL53L0X(&sensor3);

  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8,GPIO_PIN_RESET); //shut down the VL53L0X sensor 1.
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9,GPIO_PIN_RESET); //shut down the VL53L0X sensor 2.
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7,GPIO_PIN_RESET); //shut down the VL53L0X sensor 3.
  HAL_Delay(100);

#ifdef ENABLE_LASER1
	sprintf(msg,"Start Laser 1\r\n");
  HAL_UART_Transmit(&huart2, (uint8_t*)msg, strlen(msg), 0xFFFF);
	
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8,GPIO_PIN_SET); //start up the sensor1.
  HAL_Delay(24);
  if(!init(&sensor1,true)) //attempt to initialise it with the necessary settings for normal operation. Returns 0 if fail, 1 if success.
  {

  }
  else
  {
		  setTimeout(&sensor1,500);
			setAddress(&sensor1, 0x53);
			setVcselPulsePeriod(&sensor1,VcselPeriodPreRange, 18);    //  VcselPeriodPreRange:  12 to 18 (initialized default: 14)
			setVcselPulsePeriod(&sensor1,VcselPeriodFinalRange, 14);  //  VcselPeriodFinalRange: 8 to 14 (initialized default: 10)
			startContinuous(&sensor1,0);
			setMeasurementTimingBudget(&sensor1,200000);
  }
#endif
#ifdef ENABLE_LASER2	
	sprintf(msg,"Start Laser 2\r\n");
  HAL_UART_Transmit(&huart2, (uint8_t*)msg, strlen(msg), 0xFFFF);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9,GPIO_PIN_SET); //start up the sensor2.
	HAL_Delay(24);
	if(!init(&sensor2,true)) //attempt to initialise it with the necessary settings for normal operation. Returns 0 if fail, 1 if success.
  {
  }
  else
  {
			setTimeout(&sensor2,500);
			setAddress(&sensor2, 0x54);
			setVcselPulsePeriod(&sensor2,VcselPeriodPreRange, 18);    //  VcselPeriodPreRange:  12 to 18 (initialized default: 14)
			setVcselPulsePeriod(&sensor2,VcselPeriodFinalRange, 14);  //  VcselPeriodFinalRange: 8 to 14 (initialized default: 10)
			startContinuous(&sensor2,0);
			setMeasurementTimingBudget(&sensor2,200000);
  }
#endif
#ifdef ENABLE_LASER3	
	sprintf(msg,"Start Laser 3\r\n");
  HAL_UART_Transmit(&huart2, (uint8_t*)msg, strlen(msg), 0xFFFF);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7 ,GPIO_PIN_SET); //start up the sensor3.
	HAL_Delay(24);
	if(!init(&sensor3,true)) //attempt to initialise it with the necessary settings for normal operation. Returns 0 if fail, 1 if success.
  {

  }
  else
  {
			setTimeout(&sensor3,500);
			setAddress(&sensor3, 0x55);
			setVcselPulsePeriod(&sensor3,VcselPeriodPreRange, 18);    //  VcselPeriodPreRange:  12 to 18 (initialized default: 14)
			setVcselPulsePeriod(&sensor3,VcselPeriodFinalRange, 14);  //  VcselPeriodFinalRange: 8 to 14 (initialized default: 10)
			startContinuous(&sensor3,0);
			setMeasurementTimingBudget(&sensor3,200000);
  }
#endif
	HAL_Delay(100);
  /* USER CODE END 2 */

  /* Init scheduler */
  osKernelInitialize();

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of UARTParserTask */
  UARTParserTaskHandle = osThreadNew(UARTParser, NULL, &UARTParserTask_attributes);

  /* creation of PrintStatusTask */
  PrintStatusTaskHandle = osThreadNew(PrintStatus, NULL, &PrintStatusTask_attributes);

  /* creation of ReadLaserTask */
  ReadLaserTaskHandle = osThreadNew(readLaser, NULL, &ReadLaserTask_attributes);

  /* creation of UpdateMotorTask */
  UpdateMotorTaskHandle = osThreadNew(updateMotor, NULL, &UpdateMotorTask_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */

  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 180;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 2;
  RCC_OscInitStruct.PLL.PLLR = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Activate the Over-Drive mode
  */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief I2C2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C2_Init(void)
{

  /* USER CODE BEGIN I2C2_Init 0 */

  /* USER CODE END I2C2_Init 0 */

  /* USER CODE BEGIN I2C2_Init 1 */

  /* USER CODE END I2C2_Init 1 */
  hi2c2.Instance = I2C2;
  hi2c2.Init.ClockSpeed = 100000;
  hi2c2.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c2.Init.OwnAddress1 = 0;
  hi2c2.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c2.Init.OwnAddress2 = 0;
  hi2c2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c2.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C2_Init 2 */

  /* USER CODE END I2C2_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_Encoder_InitTypeDef sConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 0;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 65535;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  sConfig.EncoderMode = TIM_ENCODERMODE_TI12;
  sConfig.IC1Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC1Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC1Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC1Filter = 10;
  sConfig.IC2Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC2Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC2Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC2Filter = 10;
  if (HAL_TIM_Encoder_Init(&htim2, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_Encoder_InitTypeDef sConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 0;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 65535;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  sConfig.EncoderMode = TIM_ENCODERMODE_TI12;
  sConfig.IC1Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC1Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC1Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC1Filter = 10;
  sConfig.IC2Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC2Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC2Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC2Filter = 10;
  if (HAL_TIM_Encoder_Init(&htim3, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */

}

/**
  * @brief TIM4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM4_Init(void)
{

  /* USER CODE BEGIN TIM4_Init 0 */

  /* USER CODE END TIM4_Init 0 */

  TIM_Encoder_InitTypeDef sConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM4_Init 1 */

  /* USER CODE END TIM4_Init 1 */
  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 0;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 65535;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  sConfig.EncoderMode = TIM_ENCODERMODE_TI12;
  sConfig.IC1Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC1Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC1Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC1Filter = 10;
  sConfig.IC2Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC2Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC2Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC2Filter = 10;
  if (HAL_TIM_Encoder_Init(&htim4, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM4_Init 2 */

  /* USER CODE END TIM4_Init 2 */

}

/**
  * @brief TIM5 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM5_Init(void)
{

  /* USER CODE BEGIN TIM5_Init 0 */

  /* USER CODE END TIM5_Init 0 */

  TIM_Encoder_InitTypeDef sConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM5_Init 1 */

  /* USER CODE END TIM5_Init 1 */
  htim5.Instance = TIM5;
  htim5.Init.Prescaler = 0;
  htim5.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim5.Init.Period = 65535;
  htim5.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim5.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  sConfig.EncoderMode = TIM_ENCODERMODE_TI12;
  sConfig.IC1Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC1Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC1Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC1Filter = 10;
  sConfig.IC2Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC2Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC2Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC2Filter = 10;
  if (HAL_TIM_Encoder_Init(&htim5, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim5, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM5_Init 2 */

  /* USER CODE END TIM5_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Stream5_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream5_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream5_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, DRIVER2_AIN1_Pin|DRIVER2_AIN2_Pin|LASER3_XSHUT_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, DRIVER1_AIN2_Pin|DRIVER1_AIN1_Pin|DRIVER1_BIN2_Pin|DRIVER1_BIN1_Pin
                          |DRIVER1_STBY_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, LASER1_XSHUT_Pin|LASER2_XSHUT_Pin|DRIVER2_BIN2_Pin|DRIVER2_BIN1_Pin
                          |DRIVER2_STBY_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : DRIVER2_AIN1_Pin DRIVER2_AIN2_Pin LASER3_XSHUT_Pin */
  GPIO_InitStruct.Pin = DRIVER2_AIN1_Pin|DRIVER2_AIN2_Pin|LASER3_XSHUT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : DRIVER1_AIN2_Pin DRIVER1_AIN1_Pin DRIVER1_BIN2_Pin DRIVER1_BIN1_Pin
                           DRIVER1_STBY_Pin */
  GPIO_InitStruct.Pin = DRIVER1_AIN2_Pin|DRIVER1_AIN1_Pin|DRIVER1_BIN2_Pin|DRIVER1_BIN1_Pin
                          |DRIVER1_STBY_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : LASER1_XSHUT_Pin LASER2_XSHUT_Pin DRIVER2_BIN2_Pin DRIVER2_BIN1_Pin
                           DRIVER2_STBY_Pin */
  GPIO_InitStruct.Pin = LASER1_XSHUT_Pin|LASER2_XSHUT_Pin|DRIVER2_BIN2_Pin|DRIVER2_BIN1_Pin
                          |DRIVER2_STBY_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : ESTOP_Pin */
  GPIO_InitStruct.Pin = ESTOP_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(ESTOP_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void inverse_kinematics(float vx, float vy, float w)
{
	// 1 = 100%
	// IK
	motor_target_wVelocity[0] = 1 / WHEEL_RADIUS * (vx - vy - (LX + LY) * w);
	motor_target_wVelocity[1] = 1 / WHEEL_RADIUS * (vx + vy + (LX + LY) * w);
	motor_target_wVelocity[2] = 1 / WHEEL_RADIUS * (vx + vy - (LX + LY) * w);
	motor_target_wVelocity[3] = 1 / WHEEL_RADIUS * (vx - vy + (LX + LY) * w);
	for(int i = 0; i < WHEEL_COUNT; i++)
	{
		motor_forward[i] = motor_target_wVelocity[i] > 0 ? 1 : 0;
	}
}
void update_motor_pid(int motor, float kp, float ki, float kd)
{
	if(motor > WHEEL_COUNT)
		return;
	motor_kp[motor] = kp;
	motor_ki[motor] = ki;
	motor_kd[motor] = kd;
}
void update_motor_direction()
{
	// STBY
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, GPIO_PIN_SET); 
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_15, GPIO_PIN_SET); 
	// Motor 0 
		if(motor_target_wVelocity[0] == 0)
	{
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_RESET); 
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, GPIO_PIN_RESET); 
	}
	else if(motor_forward[0] == 1)
	{
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_RESET); 
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, GPIO_PIN_SET);  
	}
	else
	{
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_SET); 
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, GPIO_PIN_RESET); 
	}
	// Motor 1
	if(motor_target_wVelocity[1] == 0)
	{
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_RESET); 
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, GPIO_PIN_RESET); 
	}
	else if(motor_forward[1] == 1)
	{
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_RESET); 
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, GPIO_PIN_SET); 
	}
	else
	{
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_SET); 
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, GPIO_PIN_RESET); 
	}
	// Motor 2 
	if(motor_target_wVelocity[2] == 0)
	{
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_2, GPIO_PIN_RESET); 
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_3, GPIO_PIN_RESET); 
	}
	else if(motor_forward[2] == 1)
	{
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_2, GPIO_PIN_SET); 
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_3, GPIO_PIN_RESET); 
	}
	else
	{
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_2, GPIO_PIN_RESET); 
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_3, GPIO_PIN_SET); 
	}
	// Motor 3
	if(motor_target_wVelocity[3] == 0)
	{
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_14, GPIO_PIN_RESET); 
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_13, GPIO_PIN_RESET); 
	}
	else if(motor_forward[3] == 1)
	{
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_14, GPIO_PIN_RESET); 
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_13, GPIO_PIN_SET);  
	}
	else
	{
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_14, GPIO_PIN_SET); 
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_13, GPIO_PIN_RESET); 
	}
}
void update_servo_angle(int servo, float angle)
{
	// Avoid error
	if(servo < 4 && servo > 15)
		return;
	PCA9685_SetServoAngle(servo, angle);
}
int safe_unsigned_subtract(int a, int b, int limit)
{
	// assume limit = 65536
	// 0 - 65535
	if(a < b && b - a > limit)
		return(a + limit - b);
	// 65535 - 0
	if(a > b && a - b > limit)
		return(limit - a + b);
	return a - b;
}
/* USER CODE END 4 */

/* USER CODE BEGIN Header_UARTParser */
/**
  * @brief  Function implementing the UARTParserTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_UARTParser */
void UARTParser(void *argument)
{
  /* USER CODE BEGIN 5 */
	size_t dma_head = 0, dma_tail = 0;
	size_t cur_msg_sz = 0;
	uint8_t found = 0;
	uint8_t buffer[BUFFER_SIZE];
	int incomeMsg = 0;
  /* Infinite loop */
  for(;;)
  {
      do 
			{
				__disable_irq();
				dma_tail = UART_DMA_BUFFER_SIZE - huart2.hdmarx -> Instance -> NDTR;
				__enable_irq();

				if (dma_tail != dma_head) 
				{
					// Normal
					if (dma_head < dma_tail) 
					{
						for (register size_t i = dma_head; i < dma_tail; i++) 
						{
							found = (found == 0 && uart_rx_buffer[i] == '\r') ? 1 : (found == 1 && uart_rx_buffer[i] == '\n') ? 2 : 0;
							buffer[cur_msg_sz++] = uart_rx_buffer[i];
							if (found == 2) 
							{
								incomeMsg = 1;
								buffer[cur_msg_sz - 1] = '\0';
								cur_msg_sz = 0;
							}
						}
					}
					// Break into 2					
					else {
						for (register size_t i = dma_head; i < UART_DMA_BUFFER_SIZE; i++) 
						{
							found = (found == 0 && uart_rx_buffer[i] == '\r') ? 1 : (found == 1 && uart_rx_buffer[i] == '\n') ? 2 : 0;
							buffer[cur_msg_sz++] = uart_rx_buffer[i];
							if (found == 2) 
							{
								incomeMsg = 1;
								buffer[cur_msg_sz - 1] = '\0';
								cur_msg_sz = 0;
							}
						}
						for (register size_t i = 0; i < dma_tail; i++) 
						{
							found = (found == 0 && uart_rx_buffer[i] == '\r') ? 1 : (found == 1 && uart_rx_buffer[i] == '\n') ? 2 : 0;
							buffer[cur_msg_sz++] = uart_rx_buffer[i];
							if (found == 2) 
							{
								incomeMsg = 1;
								buffer[cur_msg_sz - 1] = '\0';
								cur_msg_sz = 0;
							}
						}
					}
					dma_head = dma_tail;
      }
    } while (dma_head != (UART_DMA_BUFFER_SIZE - huart2.hdmarx -> Instance -> NDTR));
    osDelay(25); // this should be the minimum time difference between each frame
		
		if(incomeMsg == 1)
		{
			incomeMsg = 0;
			
			int i = 0;
			char *p = strtok ((char*)buffer, ",");
			char *array[10];
			while (p != NULL && i < 10)
			{
					array[i++] = p;
					p = strtok (NULL, ",");
			}
			
			float vx = 0.0, vy = 0.0, w = 0.0;
			int servo_node = 0;
			float angle = 0.0;
			int motor_num = 0;
			float kp = 0, ki = 0, kd = 0;
			
			char command = *array[0];
			switch(command)
			{
				// IK
				case 'K':
				case 'k':
					vx = atof(array[1]);
					vy = atof(array[2]);
					w = atof(array[3]);
					inverse_kinematics(vx, vy, w);
					update_motor_direction();
					break;
				// Servo
				case 'S':
				case 's':
					servo_node = atoi(array[1]);
					angle = atof(array[2]);
					update_servo_angle(servo_node, angle);
					break;
				// PID
				case 'P':
				case 'p':
					motor_num = atoi(array[1]);
					kp = atof(array[2]);
					ki = atof(array[3]);
					kd = atof(array[4]);
					update_motor_pid(motor_num, kp, ki, kd);
					break;
			}
		}
  }
  /* USER CODE END 5 */
}

/* USER CODE BEGIN Header_PrintStatus */
/**
* @brief Function implementing the PrintStatusTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_PrintStatus */
void PrintStatus(void *argument)
{
  /* USER CODE BEGIN PrintStatus */
	char uart_tx_buffer[BUFFER_SIZE];
  /* Infinite loop */
  for(;;)
  {
		sprintf(uart_tx_buffer,"%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%d,%d,%d,%d,%d,%d,%d#", motor_target_wVelocity[0], motor_target_wVelocity[1], motor_target_wVelocity[2], motor_target_wVelocity[3], 
		encoder_wVelocity[0], encoder_wVelocity[1], encoder_wVelocity[2], encoder_wVelocity[3], 
		motor_duty_cycle[0],motor_duty_cycle[1],motor_duty_cycle[2],motor_duty_cycle[3],
		laser_value[0], laser_value[1], laser_value[2]);
	  HAL_UART_Transmit(&huart2, (uint8_t*)uart_tx_buffer, strlen(uart_tx_buffer), 0xFFFF);
    osDelay(10);
  }
  /* USER CODE END PrintStatus */
}

/* USER CODE BEGIN Header_readLaser */
/**
* @brief Function implementing the ReadLaserTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_readLaser */
void readLaser(void *argument)
{
  /* USER CODE BEGIN readLaser */
  /* Infinite loop */
  for(;;)
  {
		#ifdef ENABLE_LASER1
		laser_value[0] = readRangeContinuousMillimeters(&sensor1);
		#endif
		#ifdef ENABLE_LASER2
		laser_value[1] = readRangeContinuousMillimeters(&sensor2);
		#endif
		#ifdef ENABLE_LASER3
		laser_value[2] = readRangeContinuousMillimeters(&sensor3);
		#endif
    osDelay(50);
  }
  /* USER CODE END readLaser */
}

/* USER CODE BEGIN Header_updateMotor */
/**
* @brief Function implementing the UpdateMotorTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_updateMotor */
void updateMotor(void *argument)
{
  /* USER CODE BEGIN updateMotor */
	int delayMs = 10;
	int limit = 65536;
  /* Infinite loop */
  for(;;)
  {
		// Read the encoder
		// Get the w velocity
		encoder_last[0] =__HAL_TIM_GET_COUNTER(&htim2);
		encoder_last[1] =__HAL_TIM_GET_COUNTER(&htim3);
		encoder_last[2] =__HAL_TIM_GET_COUNTER(&htim4);
		encoder_last[3] =__HAL_TIM_GET_COUNTER(&htim5);
		osDelay(delayMs);
		encoder_current[0] =__HAL_TIM_GET_COUNTER(&htim2);
		encoder_current[1] =__HAL_TIM_GET_COUNTER(&htim3);
		encoder_current[2] =__HAL_TIM_GET_COUNTER(&htim4);
		encoder_current[3] =__HAL_TIM_GET_COUNTER(&htim5);
		for(int i = 0; i < WHEEL_COUNT; i++)
		{
			if(i % 2 == 0)
			{
				// 0, 2
					encoder_wVelocity[i] = -1 * safe_unsigned_subtract(encoder_current[i], encoder_last[i], limit) * RADIAN_PER_PULSE;
			}
			else
			{
				// 1, 3
					encoder_wVelocity[i] = safe_unsigned_subtract(encoder_current[i], encoder_last[i], limit) * RADIAN_PER_PULSE;
			}
			// to 1 second
			encoder_wVelocity[i] = encoder_wVelocity[i] * (1000/delayMs);
			
			// reject impossible velocity
			if(fabsf(encoder_wVelocity[i]) > ENCODER_MAX_VELOCITY)
				encoder_wVelocity[i] = encoder_last_wVelocity[i];
			
			encoder_last_wVelocity[i] = encoder_wVelocity[i];
		}

		// Update speed
		for(int i = 0; i < WHEEL_COUNT; i++)
		{			
			// reset error for 0
			if(motor_target_wVelocity[i] == 0.0)
			{
				motor_sum_error[i] = 0;
				motor_last_error[i] = 0;
				motor_duty_cycle[i] = 0;
				continue;
			}
				
 			
			// find error
			float targetPulse = motor_target_wVelocity[i] * kWheelToPCA;
			float error = motor_target_wVelocity[i] - encoder_wVelocity[i];
			if(error > -ERROR_TOLERANT && error < ERROR_TOLERANT) 
				error = 0;
			
			// calc for all error
			motor_sum_error[i] += (error * delayMs) / 1000 ;
			if(motor_sum_error[i] >= kMaxError)
				motor_sum_error[i] = kMaxError;
			else if(motor_sum_error[i] <= -kMaxError)
				motor_sum_error[i] = -kMaxError;
			float error_rate = ((error - motor_last_error[i]) / delayMs / 1000);
			
			// Apply PID
			motor_duty_cycle[i] = targetPulse + round(motor_kp[i] * error + motor_ki[i] * motor_sum_error[i] + motor_kd[i] * error_rate);
			
			// Store error
			motor_last_error[i] = error;

			PCA9685_SetPin(i, abs(motor_duty_cycle[i]), 0);
		}
  }
  /* USER CODE END updateMotor */
}

 /**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM1 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM1) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
