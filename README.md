# LGDXRobot-MCU

## About

A firmware for Low level controller. It controls motors, servo motors on the robot. Also, it reads measured distance from distance senseors.

## Usage

Using STM32CubeMX or Keil to open the project.

## Images

### Schematic Diagram for Low Level Controller

![Img1](img1.png)

### Circult Board Connection

![Img2](img2.jpg)

## Credit

This project contains driver for following modules.

* [PCA9685 Driver](https://github.com/henriheimann/stm32-hal-pca9685)
* [VL53L0X Driver](https://community.st.com/s/question/0D50X00009XkWI2/ported-vl53l0x-pololu-code-to-stm32-always-times-out-cant-find-error)
